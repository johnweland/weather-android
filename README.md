# Weather Android #

### What is this repository for? ###

* This repository is for the development and continued improvement of a simple weather app for Android.
* Version 0.01


### How do I get set up? ###

* Create a clone of the repository locally (Android Studio or Source Tree).
* Open the repository up in Android Studio (1.5 is the current version as of writing this).
* Run the Gradle Build function in Android Studio


### Contribution guidelines ###

* Code should be using all current APIs with support libraries (support for API 16 and newer as of writing this).

### Who do I talk to? ###

* john.weland@gmail.com