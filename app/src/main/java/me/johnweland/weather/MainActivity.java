package me.johnweland.weather;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            PermissionChecker permissions = PermissionChecker.getInstance(this);
            permissions.permissionsCheck();
        } else {
            // Pre-Marshmallow
            demo();
        }
    }

    // Permissions Callback (after PermissionChecker.java is ran
    @TargetApi(Build.VERSION_CODES.M)
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS:
            {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission_group.LOCATION, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for Permissions Granted
                if (perms.get(Manifest.permission_group.LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // if All Permissions Granted
                    demo();
                }
                else {
                    // if one or more Permission Denied && "don't ask me again"
                    Toast.makeText(this, R.string.permission_denied_message, Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // demo() method only to show that permission are set and app can function.
    protected void demo() {
        Toast.makeText(this, "Demo toast", Toast.LENGTH_LONG).show();
    }
}
